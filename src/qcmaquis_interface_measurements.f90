!!  dmrg-interface-utils: interface to the Maquis DMRG program for various
!!                        quantum-chemistry program packages.
!!  Copyright 2013-2018 Leon Freitag, Erik Hedegaard, Sebastian Keller,
!!                      Stefan Knecht, Yingjin Ma, Christopher Stein
!!                      and Markus Reiher
!!                      Laboratory for Physical Chemistry, ETH Zurich
!!
!!  dmrg-interface-utils is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU Lesser General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  dmrg-interface-utils is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!!  GNU Lesser General Public License for more details.
!!
!!  You should have received a copy of the GNU Lesser General Public License
!!  along with dmrg-interface-utils. If not, see <http://www.gnu.org/licenses/>.

module qcmaquis_interface_measurements

  use qcmaquis_interface_utility_routines, only : str, tri
  use qcmaquis_interface_environment, only : dmrg_external, dmrg_state
  use qcmaquis_interface_cfg, only : do_multi_meas

  implicit none

  character(len=*), parameter     :: input_lr = ' $MOLCAS/template-files/template-dmrg-gradients.maquis'
  character(len=*), parameter     :: molcaspydriver    = ' $MOLCAS/pytools/runDMRG.py '

  public compute_observables
  public extract_rdm
  public overlap_kernel

  public dmrg_import_civec_phase1
  public dmrg_import_civec_phase2


contains

! =================================================================================
!   transfer the measuring scripts (python) for later usage
! ---------------------------------------------------------------------------------
! Input  :  host_program driver
! Output :  QCMaquis measurements stored on the result*.h5 file
! =================================================================================
  subroutine compute_observables(pydriver)

    character(len=300), intent(in) :: pydriver
    integer :: status

    character(len=15) :: executable

    if(do_multi_meas) then
      executable = "dmrg_multi_meas"
    else
      executable = "dmrg_meas"
    end if

    status = system(trim(pydriver)//" --executable="//trim(executable)//' --tmpfull=$PWD/tmp'// &
                                " --output=dmrg.RDM_OUT "//" dmrg-input"           &
               )
     if (status .ne. 0 ) then
        write(6,*) "Measuring expectation values in DMRG failed."
        stop
     end if

  end subroutine compute_observables

! ======================================================================
!   Calculating 1-Density matrix, 2-Density matrix, and Spin-denstry
! ----------------------------------------------------------------------
! Input  :  maquis_model
!        :        result     maquis_name_results(iroot)   --  string
!        :    len_result                 length (integer)
!        :           spd     maquis_name_spin-density(iroot)
!        :       len_spd
!        :           DM1     maquis_name_1RDM(iroot)
!        :       len_DM1
!        :           DM2     maquis_name_2RDM(iroot)
!        :       len_DM2
!        :        CIONLY     only DMRG calculation, no orb-opt
! Output :  oneparticle.spd (then it will be renamed basing on i-th root)
!        :  oneparticle.rdm (as above)
!        :  twoparticle.rdm (as above)
! ======================================================================
  subroutine extract_rdm(result,len_result,DM1,len_DM1,DM2,len_DM2,SPD,len_SPD,cionly)

    integer                    :: len_result,len_DM1,len_DM2,len_spd, status
    character(len=len_DM1)     :: DM1
    character(len=len_DM2)     :: DM2
    character(len=len_SPD)     :: SPD
    character(len=len_result)  :: result
    logical                    :: cionly

    if(cionly)then
      !call system("$MOLCAS/pytools/rdmsave_su2_onlyone.py "//result//" ")
      status = system("$MOLCAS/pytools/rdmsave_su2.py "//result//" ")
      write(6,*)"Both 1,2 RDMs are calculated "
    else
      status = system("$MOLCAS/pytools/rdmsave_su2.py "//result//" ")
    end if

    if (status.ne.0) then
      write(6,*) "Importing RDMs in DMRG failed."
      stop
    end if

    ! For multi_meas, disable the import of spin densities
    ! as it isn't supported yet
    if (.not.do_multi_meas) then
      status = system("$MOLCAS/pytools/spdmsave_su2.py "//result//" ")
      if (status.ne.0) then
        write(6,*) "Importing spin density from DMRG failed."
        stop
      end if
    end if

    call system("mv "//"oneparticle.rdm "//DM1)

    if(.not.do_multi_meas) call system("mv "//"oneparticle.spd "//SPD)
    if(.not.cionly) call system("mv "//"twoparticle.rdm "//DM2)

  end subroutine extract_rdm

  subroutine overlap_kernel(pydriver,overlap_exe,maquis_name_states,overlap)

    character(len=300), intent(in)    :: pydriver
    character(len=300), intent(in)    :: overlap_exe
    character(len=2300),intent(inout) :: maquis_name_states(2)
    real*8,             intent(inout) :: overlap

    character(len=100)                :: overlap_between_states
    character(len=100)                :: overlap_string
    character(len=100)                :: overlap_trimmed
    integer                           :: lunit, status

    overlap_between_states = ""
    overlap_string         = ""
    lunit                  = 140

    status = system(trim(pydriver)//' --overlap '//                          &
                " --executable="//trim(overlap_exe)//                    &
                " --output=overlap.DMRG.out "//' --tmpfull=$PWD/tmp'//   &
                ' --lhs='//trim(maquis_name_states(1))//' --rhs='//      &
                trim(maquis_name_states(2))                              &
               )

    !> read data from file and store result in output variable
    open(unit=lunit,form='formatted',status='old',action="read",position='rewind',file="overlap.DMRG.out")
    read(lunit,'(a)') overlap_between_states
    read(lunit,'(a)') overlap_string
    close(lunit,status="keep")

    overlap_trimmed = trim(overlap_string)

    overlap_trimmed = overlap_trimmed(index(overlap_trimmed,'=')+1:100)
    read(overlap_trimmed,*) overlap


#ifdef _DMRG_DEBUG_
    !> debug information
    write(6,*) '  MPS1:',trim(maquis_name_states(1))
    write(6,*) '  MPS2:',trim(maquis_name_states(2))

    write(6,*) overlap_between_states,overlap
    !> end of debug information
#endif

  end subroutine overlap_kernel

!**********************************************************************
!
!                   Routines needed for SA gradients
!
!**********************************************************************
!
! Generate and read the "CI vector"
! This consists of two different subroutines that must be called separately.
!
!**********************************************************************

! Function to run QCMaquis and dump the coefficient vector for a given
! bipartition (currently, only two-site tensors at site 0 are supported,
! other bipartitions will be added in the future.
! Correct usage: get the length of the "CI vector" by running
! dmrg_import_civec_phase1, then allocate an array externally with the
! correct length and call dmrg_import_civec_phase2
! Input: checkpoint: checkpoint name
! iroot: root # (only for the temporary output suffix)
! Return: length of the CI coefficient. If < 0, an error has occurred.
! Leaves the file "twositetensordump.X" with X=iroot in scratch, which
! should be read by dmrg_import_civec_phase2 (and will be deleted)
  integer function dmrg_import_civec_phase1(checkpoint,iroot) result(res)

    character(len=*), intent(in)    :: checkpoint
    integer, intent(in)             :: iroot

    integer                         :: status, idx
    integer, parameter              :: lunit = 369
    character(len=*), parameter     :: pyreadscript    = ' $MOLCAS/pytools/twositetensor_dump.py '
    character(len=2000)             :: currdir, prj, full_cp_name, res_name, dmrgpy_options
    character(len=50)               :: civecfile
    ! temporary variables to read the file
    ! here we're not really interested in their values
    integer             :: i, j, k
    real*8              :: value

    res = -1

    ! prepare full checkpoint and result names
    call getenv("CurrDir",currdir)
    call getenv("Project",prj)

    full_cp_name = trim(currdir)//'/'//trim(checkpoint)
    ! we don't use file_name_generator because some of the variables used there may be unitialised in MCLR
    ! save the TDMs into the result.h5 file of root iroot
    res_name = trim(currdir)//'/'//trim(prj)//".results_state."//trim(str(iroot))//".h5"

    dmrgpy_options = '--notransform --executable=dmrg_meas '//                                          &
                        '--replace="orbital_number='//trim(str(dmrg_external%norb))//                   &
                      '" --replace="irrep_in_pointgroup='//trim(str(dmrg_state%irefsm-1))//             &
                      '" --replace="electron_number_total= '//trim(str(dmrg_state%nactel))//            &
                      '" --replace="ms2='//trim(str(dmrg_state%ms2))//                                  &
                      '" --replace="ket_checkpoint='//trim(full_cp_name)//                              &
                      '" --replace="saved_result='//trim(res_name)//                                    &
                      '" --replace="lrparamsite='//trim(str(dmrg_external%lrsite))//                    &
                      '" --replace="lrparamtwosite='//trim(str(transfer(dmrg_external%lr_twosite,1)))// &
                      '" --replace="cifile=delete-line" --replace="sigmavec=delete-line" '//            &
                      '  --replace="h-diag=delete-line" '//                                             &
                        '--replace="dumptst=1" --input=dmrg-input-mpspardump --output=dmrg.out_mpspar'


    write(6,*) 'Extracting MPS parameters from state '//trim(checkpoint)

    ! run dmrg_meas
    status = system(molcaspydriver//trim(dmrgpy_options)//trim(input_lr))
    if (int(status).ne.0) then
      write(6,*)"DMRG run for extracting MPS parameters from checkpoint "//trim(checkpoint)//" failed"
      stop
    end if

    ! run python script to read the QCMaquis results.h5 file
    status = system(pyreadscript//trim(res_name)//" "//trim(str(iroot)))
    if (status.ne.0) then
      write(6,*) "Extracting MPS parameters from checkpoint "//trim(checkpoint)//" failed"
      stop
    end if

    !read the file. we only need the length of the file
    civecfile = trim('twositetensordump.'//trim(str(iroot)))
    open(unit=lunit,file=trim(civecfile))
    idx = 0
    do
      read(lunit,*,iostat=status) value, i, j, k
      if (is_iostat_end(status)) exit
      if (status > 0) then
        write(6,*) 'problem reading MPS parameters from file '//trim(civecfile)
        stop
      end if
      idx = idx + 1
    end do
    close(lunit)

    if (idx > 0) res = idx
  end function dmrg_import_civec_phase1

  ! given the externally allocated vector civec with correct length obtained from a _phase1 call,
  ! read the MPS parameter vector into civec for a given root iroot

  ! input: iroot -- root for which the MPS parameter vector should be read
  !        ncivec -- length of civec (must be passed explicitly because MCLR works with
  !                  old-style assumed-size arrays)
  ! output: civec -- vector with MPS parameters
  subroutine dmrg_import_civec_phase2(iroot,ncivec,civec)

    integer, intent(in) :: iroot, ncivec
    real*8, intent(out) :: civec(*)

    integer, parameter  :: lunit = 370

    character(len=50)   :: civecfile
    integer             :: status
    ! temporary variables for reading the file
    integer             :: i, j, k, idx
    real*8              :: value

    idx = 0

    civecfile = trim('twositetensordump.'//trim(str(iroot)))
    open(unit=lunit,file=trim(civecfile))
    do
      read(lunit,*,iostat=status) value, i,j,k
      idx = idx + 1
      if (is_iostat_end(status)) exit
      if (status > 0) then
        write(6,*) 'problem reading MPS parameters from file '//trim(civecfile)
        stop
      end if
      if (idx > ncivec) then
        write(6,*) 'Size mismatch in MPS parameter file '//trim(civecfile)
        stop
      end if
      civec(idx) = value
    end do
    close(lunit)

  end subroutine dmrg_import_civec_phase2

  !!! Import diagonal local Hamiltonian matrix elements
  !!! Input: checkpoint: checkpoint name
  !!!        iroot : root #
  !!!        veclen: length of the resulting vector (result of dmrg_import_civec_phase1)
  !!! Output: Hdiag: Vector with Hamiltonian diagonal matrix elements
  subroutine dmrg_import_local_Hdiag(checkpoint,iroot,veclen,Hdiag)
    character(len=*), intent(in)    :: checkpoint
    integer, intent(in)             :: iroot,veclen
    real*8, intent(out)             :: Hdiag(*)

    character(len=2000)             :: currdir, prj, full_cp_name, res_name, dmrgpy_options

    character(len=*), parameter     :: pyreadscript    = ' $MOLCAS/pytools/local_hamiltonian.py '
    character(len=*), parameter     :: Hfile = 'local_hamiltonian.txt'

    ! variables needed for reading the text file
    integer, parameter              :: lunit = 369
    integer                         :: idx, status
    real*8                          :: value
    ! in these routines we need to call a separate dmrg_meas binary from the linear_response_clean branch
    ! at least until the additional features get merged into the main branch

    ! prepare full checkpoint and result names
    call getenv("CurrDir",currdir)
    call getenv("Project",prj)

    full_cp_name = trim(currdir)//'/'//trim(checkpoint)
    ! we don't use file_name_generator because some of the variables used there may be unitialised in MCLR
    ! save the TDMs into the result.h5 file of root iroot
    res_name = trim(currdir)//'/'//trim(prj)//".results_state."//trim(str(iroot))//".h5"

    dmrgpy_options = '--notransform --executable=dmrg_meas '//                                          &
                        '--replace="orbital_number='//trim(str(dmrg_external%norb))//                   &
                      '" --replace="irrep_in_pointgroup='//trim(str(dmrg_state%irefsm-1))//             &
                      '" --replace="electron_number_total= '//trim(str(dmrg_state%nactel))//            &
                      '" --replace="ms2='//trim(str(dmrg_state%ms2))//                                  &
                      '" --replace="ket_checkpoint='//trim(full_cp_name)//                              &
                      '" --replace="saved_result='//trim(res_name)//                                    &
                      '" --replace="lrparamsite='//trim(str(dmrg_external%lrsite))//                    &
                      '" --replace="lrparamtwosite='//trim(str(transfer(dmrg_external%lr_twosite,1)))// &
                      '" --replace="cifile=delete-line" --replace="sigmavec=delete-line" '//            &
                      '  --replace="num_states=delete-line"'//                                          &
                      '  --replace="dumptst=delete-line" '//                                            &
                        '--replace="h-diag=1" --input=dmrg-input-h-diag --output=dmrg.out_h'


    write(6,*) 'Extracting local Hamiltonian matrix elements from state '//trim(checkpoint)

    ! run dmrg_meas
    status = system(molcaspydriver//trim(dmrgpy_options)//trim(input_lr))
    if (status.ne.0) then
      write(6,*) "DMRG run for extracting local Hamiltonian from checkpoint "//trim(checkpoint)//" failed"
      stop
    end if

    ! run python script to read the QCMaquis results.h5 file
    status = system(pyreadscript//trim(res_name)//" -d")
    if (status.ne.0) then
      write(6,*) "Extracting local Hamiltonian from checkpoint "//trim(checkpoint)//" failed"
      stop
    end if

    open(unit=lunit,file=trim(Hfile))
    idx = 0
    do
      read(lunit,*,iostat=status) value
      if (is_iostat_end(status)) exit
      if (status > 0) then
        write(6,*) 'problem reading local Hamiltonian from file '//trim(Hfile)
        stop
      end if
      idx = idx + 1
      if (idx > veclen) stop 'the size of the local Hamiltonian does not match!'
      Hdiag(idx) = value
    end do
    close(lunit)
  end subroutine dmrg_import_local_Hdiag

  !!! Import a sigma vector with a modified FCIDUMP file
  !!! Input:  checkpoint: checkpoint file name
  !!!            fcidump: fcidump file name
  !!!             veclen: length of vec (and vec_aux)
  !!! (optional) vec_aux: external coefficient vector for the sigma build.
  !!! Output: vec: sigma vector
  subroutine dmrg_import_sigmavec(checkpoint,fcidump,vec,veclen,vec_aux)
    character(len=*), intent(in)    :: checkpoint, fcidump
    integer, intent(in)             :: veclen
    real*8, intent(in), optional    :: vec_aux(veclen)
    real*8, intent(out)             :: vec(veclen)

    character(len=2000)             :: currdir, prj, full_cp_name, fcidump_name,res_name, vec_name, dmrgpy_options

    character(len=*), parameter     :: pyreadscript    = ' $MOLCAS/pytools/local_hamiltonian.py '
    character(len=*), parameter     :: Hfile = 'local_hamiltonian.txt'

    ! random number to attach to the file name
    real*8                          :: rnd
    character(len=10)               :: rndtag
    ! variables needed for reading the text file
    integer, parameter              :: lunit = 372
    integer                         :: idx, status, i
    real*8                          :: value

    logical                         :: aux_build

    ! in these routines we need to call a separate dmrg_meas binary from the linear_response_clean branch
    ! at least until the additional features get merged into the main branch

    aux_build = present(vec_aux)

    ! prepare full checkpoint and result names
    call getenv("CurrDir",currdir)
    call getenv("Project",prj)

    full_cp_name = trim(currdir)//'/'//trim(checkpoint)

    ! result file will be saved in scratch directory and have a random number appended to its name
    call random_number(rnd)
    rndtag = trim(str(floor(10000*rnd)))
    res_name = "sigma_vec_update."//trim(rndtag)//".h5"

    if(aux_build) then
      vec_name = "mps_parameters"//trim(rndtag)
      open(unit=lunit,file=trim(vec_name))
      write (lunit, *) (vec_aux(i), i = 1, veclen)
      close(lunit)
    else
      vec_name = "1"
    end if

    fcidump_name = fcidump

    dmrgpy_options = '--notransform --executable=dmrg_meas '//                                          &
                        '--replace="orbital_number='//trim(str(dmrg_external%norb))//                   &
                      '" --replace="irrep_in_pointgroup='//trim(str(dmrg_state%irefsm-1))//             &
                      '" --replace="electron_number_total= '//trim(str(dmrg_state%nactel))//            &
                      '" --replace="ms2='//trim(str(dmrg_state%ms2))//                                  &
                      '" --replace="ket_checkpoint='//trim(full_cp_name)//                              &
                      '" --replace="saved_result='//trim(res_name)//                                    &
                      '" --replace="lrparamsite='//trim(str(dmrg_external%lrsite))//                    &
                      '" --replace="lrparamtwosite='//trim(str(transfer(dmrg_external%lr_twosite,1)))// &
                      '" --replace="cifile=delete-line"'//                                              &
                      '  --replace="sigmavec='//trim(vec_name)//'" '//                                  &
                      '  --replace="dumptst=delete-line" '//                                            &
                      '  --replace="num_states=delete-line" '//                                         &
                      '  --replace="FCIDUMP='//trim(fcidump_name)//'" '//                               &
                        '--replace="h-diag=delete-line" --input=dmrg-input-sigmavec '//                 &
                        '--output=dmrg.out_sigmavec'


    write(6,*) 'Performing modified sigma vector build from state '//trim(checkpoint)
    if (aux_build)  write(6,*) 'reading auxiliary CI vector from file '//trim(vec_name)

    ! run dmrg_meas
    status = system(molcaspydriver//trim(dmrgpy_options)//trim(input_lr))
    if (status.ne.0) then
      write(6,*) "DMRG run for sigma vector build for state "//trim(checkpoint)//" failed"
      stop
    end if

    ! delete the FCIDUMP file
!     open(unit=lunit_,file=trim(fcidump_name))
!     close(lunit,status='delete')

    if (aux_build) then
      ! delete the MPS parameter file
      open(unit=lunit,file=trim(vec_name))
      close(lunit,status='delete')
    end if

    ! run python script to read the QCMaquis results.h5 file
    status = system(pyreadscript//trim(res_name)//" -s")
    if (status.ne.0) then
      write(6,*) "Extracting sigma vector for checkpoint "//trim(checkpoint)//" failed"
      stop
    end if

    open(unit=lunit,file=trim(Hfile))
    idx = 0
    do
      read(lunit,*,iostat=status) value
      if (is_iostat_end(status)) exit
      if (status > 0) then
        write(6,*) 'problem reading sigma vector from file '//trim(Hfile)
        stop
      end if

      idx = idx + 1
      if (idx > veclen) stop 'the size of the sigma vector does not match!'
      vec(idx) = value
    end do
    close(lunit)

    ! delete the result h5 file produced by QCMaquis
    open(unit=lunit, file=trim(res_name))
    close(lunit,status='delete')
  end subroutine dmrg_import_sigmavec

  !!! Import 1- and 2-RDMs with updated MPS parameters ("CI coefficients")
  !!! Input: checkpoint: checkpoint name
  !!!        ext_vec: vector of MPS parameters supplied externally
  !!!        veclen: length of ext_vec (number of MPS parameters including the redundant ones)
  !!! (again, we don't use assumed-shape arrays because of old code in MCLR)
  !!! Output: dv: "updated" 1-RDM <ext_vec|c+c|checkpoint> (symmetrised but in full form!)
  !!!         pv: "updated" 2-RDM (symmetrised) in half-triangular form
  subroutine dmrg_import_rdm_update(checkpoint,ext_vec,veclen,dv,pv)

      character(len=*), intent(in)    :: checkpoint
      real*8, intent(in)    :: ext_vec(*)
      integer,intent(in)    :: veclen

      real*8, intent(out)   :: dv(*) ! dim = nact**2
      real*8, intent(out)   :: pv(*) ! dim = nact**2*(nact**2-1)/2
      !----------------------------------------------
      integer                         :: nact,i,j,k,l, ij1, ij2, kl1, kl2
      real*8                          :: value
      integer                         :: status
      integer, parameter              :: lunit = 371, lunit_vec = 1371
      character(len=*), parameter     :: pyreadscript    = ' $MOLCAS/pytools/tdmsave_su2.py '
      character(len=2000)             :: currdir, prj, full_cp_name
      character(len=2000)             :: dmrgpy_options
      character(len=100)              :: res_name, vec_name, RDMfile
      ! random number to attach to the file name
      real*8                          :: rnd
      character(len=10)               :: rndtag

      ! scratch matrices for reading the full form and symmetrisation
      real*8, allocatable             :: tdm1sym(:,:), tdm2sym(:)

      ! prepare full checkpoint and result names
      call getenv("CurrDir",currdir)
      call getenv("Project",prj)

      full_cp_name = trim(currdir)//'/'//trim(checkpoint)

      ! result file will be saved in scratch directory and have a random number appended to its name
      call random_number(rnd)
      rndtag = trim(str(floor(10000*rnd)))
      res_name = "lagrange_update."//trim(rndtag)//".h5"
      vec_name = "mps_parameters"//trim(rndtag)

      dmrgpy_options = '--notransform --executable=dmrg_meas '//                                    &
                    '--replace="orbital_number='//trim(str(dmrg_external%norb))//                   &
                  '" --replace="irrep_in_pointgroup='//trim(str(dmrg_state%irefsm-1))//             &
                  '" --replace="electron_number_total= '//trim(str(dmrg_state%nactel))//            &
                  '" --replace="ms2='//trim(str(dmrg_state%ms2))//                                  &
                  '" --replace="ket_checkpoint='//trim(full_cp_name)//                              &
                  '" --replace="saved_result='//trim(res_name)//                                    &
                  '" --replace="lrparamsite='//trim(str(dmrg_external%lrsite))//                    &
                  '" --replace="lrparamtwosite='//trim(str(transfer(dmrg_external%lr_twosite,1)))// &
                  '" --replace="cifile='//trim(vec_name)//                                          &
                  '" --replace="sigmavec=delete-line"  --replace="dumptst=delete-line" '//          &
                  ' --replace="h-diag=delete-line"'//                                               &
                  ' --input=dmrg-input-lagrange-update --output=dmrg.out_lagrange-update'

                    !--replace="cifile=delete-line"
      write(6,*) 'Calculating updated Lagrange RDMs for state '//trim(checkpoint)

      ! create the file with the external MPS parameters to be read by QCMaquis
      open(unit=lunit_vec,file=trim(vec_name))
      write (lunit_vec, '(E26.19)') (ext_vec(i), i = 1, veclen)
      close(lunit_vec)

      ! run dmrg_meas
      status = system(molcaspydriver//trim(dmrgpy_options)//trim(input_lr))
      if (status.ne.0) then
        write(6,*) "DMRG run for updated Lagrange RDMs "//trim(checkpoint)//" failed"
        stop
      end if

      ! delete the MPS parameter file
      open(unit=lunit_vec,file=trim(vec_name))
      close(lunit_vec,status='delete')

      ! run python script to read the QCMaquis results.h5 file
      status = system(pyreadscript//trim(res_name)//" -l "//trim(rndtag))
      if (status.ne.0) then
        write(6,*) "Extracting updated Lagrange RDMs for checkpoint "//trim(checkpoint)//" failed"
        stop
      end if

      ! delete the result h5 file produced by QCMaquis
      open(unit=lunit_vec, file=trim(res_name))
      close(lunit_vec,status='delete')

      ! copy-paste from dmrg_task_import_rdmT

      ! read 1-TDM (in square form)

      RDMfile='onerdmlagrange.'//trim(rndtag)

      open(unit=lunit,file=trim(RDMfile))
      read(lunit,*)nact

      allocate(tdm1sym(nact,nact))
      tdm1sym = 0.0d0

      do
        read(lunit,*,iostat=status) i,j,value
        if (is_iostat_end(status)) exit
        if (status > 0) then
          write(6,*) 'problem reading 1-TDM from file '//trim(RDMfile)
          stop
        end if
!         dv(i+1+j*nact) = value
        tdm1sym(i+1,j+1) = value
      end do
      close(lunit, status="delete")

      ! symmetrise 1-TDM
      do i=1, nact
      do j=1, i
        dv(i+nact*(j-1)) = tdm1sym(i,j)+tdm1sym(j,i)
        dv(j+nact*(i-1)) = tdm1sym(i,j)+tdm1sym(j,i)
      enddo
      enddo
      if (allocated(tdm1sym)) deallocate(tdm1sym)

      ! read the 2-TDM
      RDMfile='twordmlagrange.'//trim(rndtag)

      open(unit=lunit,file=trim(RDMfile))
      read(lunit,*)nact

!       allocate(tdm2syma(nact,nact,nact,nact))
! dimensions to match MOLCAS -- i.e. half-symmetric
      allocate(tdm2sym(nact**2*(nact**2+1)/2))
      tdm2sym = 0.0d0
      do
        read(lunit,*,iostat=status) i,j,k,l,value
        if (is_iostat_end(status)) exit
        if (status > 0) then
          write(6,*) 'problem reading 2-TDM from file '//trim(RDMfile)
          stop
        end if
        ij1 = 1+i+nact*l
        kl1 = 1+j+nact*k
        tdm2sym(tri(ij1,kl1)) = value * 2.0d0
      end do
      close(lunit, status="delete")

      ! symmetrise
      do i = 1, nact
      do j = 1, nact
      do k = 1, nact
      do l = 1, nact
        ij1=nact*(i-1)+j
        ij2=nact*(j-1)+i
        kl1=nact*(k-1)+l
        kl2=nact*(l-1)+k
        if (ij1.ge.kl1) pv(tri(ij1,kl1))=tdm2sym(tri(ij1,kl1))+tdm2sym(tri(ij2,kl2))
      end do
      end do
      end do
      end do


      if (allocated(tdm2sym)) deallocate(tdm2sym)
  end subroutine dmrg_import_rdm_update

  !!! Import average 1- and 2-RDMs with updated MPS parameters ("CI coefficients")
  !!! from dmrg_multi_meas
  !!! Input: checkpoints: checkpoint names
  !!!        ext_vec: super-vector of MPS parameters supplied externally
  !!!        veclen: length of ext_vec (number of MPS parameters including the redundant ones, pro state)
  !!! (again, we don't use assumed-shape arrays because of old code in MCLR)
  !!! Output: dv: average "updated" 1-RDM <ext_vec|c+c|checkpoint> (symmetrised but in full form!)
  !!!         pv: average "updated" 2-RDM (symmetrised) in half-triangular form
  subroutine dmrg_import_average_rdm_update(checkpoints,ext_vec,veclen,dv,pv)

      use qcmaquis_interface_cfg
      character(len=*),dimension(:), intent(in)    :: checkpoints
      real*8, intent(in)    :: ext_vec(*)
      integer,intent(in)    :: veclen

      real*8, intent(out)   :: dv(*) ! dim = nact**2
      real*8, intent(out)   :: pv(*) ! dim = nact**2*(nact**2-1)/2
      !----------------------------------------------
      integer                         :: nact,i,j,k,l, ij1, ij2, kl1, kl2,iroot
      real*8                          :: value
      integer                         :: status
      integer, parameter              :: lunit = 371, lunit_vec = 1371
      character(len=*), parameter     :: pyreadscript    = ' $MOLCAS/pytools/tdmsave_su2.py '
      character(len=2000)             :: currdir, prj, &
                                         res_name, vec_name, RDMfile ! needed for a continuous string to write to the input template
      character(len=4095)             :: dmrgpy_options, full_cp_name
      character(len=100),allocatable  :: res_names(:), vec_names(:)
      ! random number to attach to the file name
      real*8                          :: rnd
      character(len=10)               :: rndtag

      ! scratch matrices for reading the full form and symmetrisation
      real*8, allocatable             :: tdm1sym(:,:), tdm2sym(:)

      integer :: nroots

      nroots = dmrg_state%nroot

      allocate(res_names(nroots), vec_names(nroots))

      ! prepare full checkpoint and result names
      call getenv("CurrDir",currdir)
      call getenv("Project",prj)

      full_cp_name = ''
      res_name = ''
      vec_name = ''

      do iroot=1,nroots


        ! result file will be saved in scratch directory and have a random number appended to its name
        call random_number(rnd)
        rndtag = trim(str(floor(10000*rnd)))
        res_names(iroot) = "lagrange_update."//trim(rndtag)//".h5"
        vec_names(iroot) = "mps_parameters"//trim(rndtag)

        if (iroot.eq.1) then
          full_cp_name = trim(currdir)//'/'//trim(checkpoints(iroot))
          res_name = trim(res_names(iroot))
          vec_name = trim(vec_names(iroot))
        else
          full_cp_name = trim(full_cp_name)//';'//trim(currdir)//'/'//trim(checkpoints(iroot))
          res_name = trim(res_name)//';'//trim(res_names(iroot))
          vec_name = trim(vec_name)//';'//trim(vec_names(iroot))
        end if

        ! create the file with the external MPS parameters to be read by QCMaquis
        open(unit=lunit_vec,file=trim(vec_names(iroot)))
        write (lunit_vec, '(E26.19)') (ext_vec(i), i = (iroot-1)*veclen+1, iroot*veclen)
        close(lunit_vec)
      end do

      dmrgpy_options = '--notransform --executable=dmrg_multi_meas '//                              &
                    '--replace="orbital_number='//trim(str(dmrg_external%norb))//                   &
                  '" --replace="irrep_in_pointgroup='//trim(str(dmrg_state%irefsm-1))//             &
                  '" --replace="electron_number_total= '//trim(str(dmrg_state%nactel))//            &
                  '" --replace="ms2='//trim(str(dmrg_state%ms2))//                                  &
                  '" --replace="ket_checkpoint='//trim(full_cp_name)//                              &
                  '" --replace="saved_result='//trim(res_name)//                                    &
                  '" --replace="lrparamsite='//trim(str(dmrg_external%lrsite))//                    &
                  '" --replace="lrparamtwosite='//trim(str(transfer(dmrg_external%lr_twosite,1)))// &
                  '" --replace="cifile='//trim(vec_name)//                                          &
                  '" --replace="sigmavec=delete-line"  --replace="dumptst=delete-line" '//          &
                  ' --replace="h-diag=delete-line"'//                                               &
                  ' --replace="num_states='//trim(str(nroots))//'"'//                               &
                  ' --input=dmrg-input-lagrange-update --output=dmrg.out_lagrange-update'


      write(6,'(A)', ADVANCE='no') 'Calculating updated Lagrange RDMs for states '
      write(6,*) (trim(checkpoints(iroot))//" ", iroot=1,nroots)


      ! run dmrg_meas
      status = system(molcaspydriver//trim(dmrgpy_options)//trim(input_lr))
      if (status.ne.0) then
        write(6,*) "DMRG run for updated Lagrange RDMs "//trim(full_cp_name)//" failed"
        stop
      end if

      ! read all the results

      nact = dmrg_external%norb
      ! initialise output matrices: 1-RDM
      do i=1, nact
      do j=1, i
        dv(i+nact*(j-1)) = 0.0d0
        dv(j+nact*(i-1)) = 0.0d0
      enddo
      enddo
      ! 2-RDM
      do i = 1, nact
      do j = 1, nact
      do k = 1, nact
      do l = 1, nact
        ij1=nact*(i-1)+j
        kl1=nact*(k-1)+l
        if (ij1.ge.kl1) pv(tri(ij1,kl1))=0.0d0
      end do
      end do
      end do
      end do
!
      ! allocate matrices for state-specific non-symmetric TDMs
      allocate(tdm1sym(nact,nact))
      !       allocate(tdm2syma(nact,nact,nact,nact))
      ! dimensions to match MOLCAS -- i.e. half-symmetric
      allocate(tdm2sym(nact**2*(nact**2+1)/2))

      do iroot=1,nroots
        ! delete the MPS parameter file
        open(unit=lunit_vec,file=trim(vec_names(iroot)))
        close(lunit_vec,status='delete')

        ! run python script to read the QCMaquis results.h5 file
        status = system(pyreadscript//trim(res_names(iroot))//" -l "//trim(str(iroot)))
        if (status.ne.0) then
          write(6,*) "Extracting updated Lagrange RDMs for checkpoint "//trim(checkpoints(iroot))//" failed"
          stop
        end if

        ! delete the result h5 file produced by QCMaquis
        open(unit=lunit_vec, file=trim(res_names(iroot)))
        close(lunit_vec,status='delete')

        ! copy-paste from dmrg_task_import_rdmT

        ! read 1-TDM (in square form)

        RDMfile='onerdmlagrange.'//trim(str(iroot))

        open(unit=lunit,file=trim(RDMfile))
        read(lunit,*)nact !! this must be the same as dmrg_external%norb!
        if (nact.ne.dmrg_external%norb) then
          write(6,*) 'Number of active orbitals in RDM file is not correct!'
          stop
        end if

        tdm1sym = 0.0d0

        do
          read(lunit,*,iostat=status) i,j,value
          if (is_iostat_end(status)) exit
          if (status > 0) then
            write(6,*) 'problem reading 1-TDM from file '//trim(RDMfile)
            stop
          end if
  !         dv(i+1+j*nact) = value
          tdm1sym(i+1,j+1) = value
        end do
        close(lunit, status="delete")

        ! symmetrise 1-TDM and make average
        do i=1, nact
        do j=1, i
          value = dv(i+nact*(j-1))+(tdm1sym(i,j)+tdm1sym(j,i))/nroots
          dv(i+nact*(j-1)) = value
          dv(j+nact*(i-1)) = value
        enddo
        enddo

        ! read the 2-TDM
        RDMfile='twordmlagrange.'//trim(str(iroot))

        open(unit=lunit,file=trim(RDMfile))
        read(lunit,*)nact
        if (nact.ne.dmrg_external%norb) then
          write(6,*) 'Number of active orbitals in RDM file is not correct!'
          stop
        end if

        tdm2sym = 0.0d0
        do
          read(lunit,*,iostat=status) i,j,k,l,value
          if (is_iostat_end(status)) exit
          if (status > 0) then
            write(6,*) 'problem reading 2-TDM from file '//trim(RDMfile)
            stop
          end if
          ij1 = 1+i+nact*l
          kl1 = 1+j+nact*k
          tdm2sym(tri(ij1,kl1)) = value * 2.0d0
        end do
        close(lunit, status="delete")

        ! symmetrise
        do i = 1, nact
        do j = 1, nact
        do k = 1, nact
        do l = 1, nact
          ij1=nact*(i-1)+j
          ij2=nact*(j-1)+i
          kl1=nact*(k-1)+l
          kl2=nact*(l-1)+k
          if (ij1.ge.kl1) pv(tri(ij1,kl1))=pv(tri(ij1,kl1))+(tdm2sym(tri(ij1,kl1))+tdm2sym(tri(ij2,kl2)))/nroots
        end do
        end do
        end do
        end do

      end do ! loop over states

      if (allocated(tdm1sym)) deallocate(tdm1sym)
      if (allocated(tdm2sym)) deallocate(tdm2sym)

      if(allocated(res_names)) deallocate(res_names)
      if(allocated(vec_names)) deallocate(vec_names)
  end subroutine dmrg_import_average_rdm_update
end module qcmaquis_interface_measurements

