// number of orbitals on lattice => lattice.size() in QCMaquis
L            = orbital_number

// target irrep of point group for a given state
irrep        =  irrep_in_pointgroup
// total number of electrons
nelec        =  electron_number_total
// spin state 2*S: 2*S=0 (singlet); 2*S=1 (doublet); 2*S=2 (triplet), ...
spin         =  ms2

chkpfile     = 'ket_checkpoint'
resultfile   = 'saved_result'
storagedir = './tmp'

symmetry = 'su2u1pg'

// Input for mps_multi_canonize, ignored by dmrg_meas (and deleted by the interface)
checkpoints     = "in_chk"
out_checkpoints = "out_chk"

// MEASUREMENTS/expectation value calculations (via dmrg_meas)
// specific for gradient calculations. Will be enabled/disabled by the interface

// 1-RDM with an updated coefficient file
MEASURE[1rdm-lagrangeL] = "cifile"
// MEASURE[1rdm-lagrangeR] = "cifile"
// 2-RDM with an updated coefficient file
MEASURE[2rdm-lagrangeL] = "cifile"
// MEASURE[2rdm-lagrangeR] = "cifile"

// "CI coefficient" dump (i.e. two-site tensor dump if we use two-site DMRG MPS parameters
MEASURE[dump-tst] = "dumptst"

// Local Hamiltonian diagonal dump for the MCLR preconditioner
MEASURE[local-hamiltonian-diag] = "h-diag"

// Sigma vector (i.e. H.c) dump using an externally supplied file with DMRG MPS parameters
MEASURE[sigma-vector] = "sigmavec"

// DMRG Site at which DMRG LR parameters should be extracted
lrparam_site = lrparamsite

// Use two-site DMRG parameters (0 = false, default = true)
lrparam_twosite = lrparamtwosite


// FIXED SETTINGS - DO NOT CHANGE
LATTICE                  = "orbitals"
CONSERVED_QUANTUMNUMBERS = "Nup,Ndown"
MODEL                    = "quantum_chemistry"
integral_file            = "FCIDUMP"
lattice_library          = "coded"
model_library            = "coded"
integral_cutoff          = 1e-300

n_states_sa = num_states
