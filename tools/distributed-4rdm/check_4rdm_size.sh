#!/bin/bash

## This scripts checks whether a 4-RDM calculation has succeeded by doing some simple consistency checks.
## (C) 2018 Leon Freitag

subdir=parts/


oldpwd=$PWD
cd $subdir
missing=0
sum=0
missingparts=()
echo "Part 1 -- checking for fourparticle.rdm.* files..."

for i in part-*; do
  if [ ! -e $i/fourparticle.rdm.* ]; then
     if [ $missing -eq 0 ]; then
       missing=1
       echo "Some jobs failed: missing parts:"
     fi 
     echo $i
     missingparts+=($i)
  fi
done 

if [ $missing -eq 0 ]; then
  echo "Part 1 passed."
  echo "Part 2 -- checking for file size consistency ..."
  totalsize=$(for i in part-*; do ls -la $i/fourparticle.rdm.*; done | awk 'BEGIN{sum=0} { sum += $5 } END {print sum}')
  collectedsize=$(ls -la fourparticle.rdm* | awk '{print $5}')
  if [ $totalsize -ne $collectedsize ]; then
    echo "File size of fourparticle.rdm.0.0 does not match the sum of its parts!"
    echo "Something went wrong with merging the parts (or GNU parallel does not work properly)"
    echo "Run "
    echo "for i in part-*; do cat $i/fourparticle* >> fourpt2; done; mv fourpt2 fourparticle.rdm.X.X"
    echo "to obtain the full 4-RDM."
    sum=1
  else
    echo "fourparticle.rdm.X.X file size is OK."
  fi
else
  echo "Part 2a -- checking which calculation crashed while evaluating the RDM ..."
  missingrdm=()
  for i in ${missingparts[@]}; do h5dump $i/*result*h5 | grep fourptdm > /dev/null; if [ $? -ne 0 ]; then missingrdm+=($i); fi; done
  if [ ${#missingrdm[@]} -eq 0 ]; then
    echo "None. Something wrong went with the fourptdm_perm.py script, please re-run it for the parts above to obtain fourparticle.rdm.X.X files"
  else
    echo "The following calculation crashed and need to be re-run:"
    echo ${missingrdm[@]}
  fi
fi

cd $oldpwd
