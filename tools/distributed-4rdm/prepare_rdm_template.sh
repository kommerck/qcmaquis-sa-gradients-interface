#!/bin/bash
#set -x
t3rdmmode=0
spin=0
states=()
submitfile=$MOLCAS/Tools/distributed-4rdm/submit-4rdm.sh
jobmanager=$MOLCAS/pytools/jobmanager.py

command_usage()
{
  echo "Usage: "
	echo " $0 [ -3 ] [ -s <SPIN> ] <state range> "
	echo " $0 prepares the scratch directory for the distributed 4-RDM or t-3-RDM evaluation."
	echo " -3 -- prepare t-3RDM evaluation instead of 4-RDM"
	echo " -s <SPIN> -- use the checkpoint of the wavefunction with the total spin S=SPIN, if multiple are available (0 -- singlet, 1 -- doublet, 2 -- triplet etc.) Default -- singlet."
	echo " <state range> (optional) list of states for which the directories should be prepared. Default: all states"
}

function checkpoint_found()
{
  local __molcasproject=$1
  local __state=$2
  local __spin=$3
  local chkpfound=0
  local checkpointstate=$(ls -d -1 ${__molcasproject}.checkpoint_state.${__state}.${__spin}.${__spin}.h5 | head -1)
  if [ ! -d $checkpointstate ]; then
    checkpointstate=$(ls -d -1 ${__molcasproject}.checkpoint_state.${__state}.*.h5 | head -1)
    if [ ! -d $checkpointstate ]; then
      chkpfound=0
    else
      chkpfound=1
    fi
  else
    chkpfound=1
  fi

  #echo ${__molcasproject} ${__state} ${__spin}
  if [ ${chkpfound} ]; then
    echo ${checkpointstate}
  else
    echo "MPS Checkpoint for state ${__state} expected but not found! Exiting..."
    exit 1
  fi
}

if [ $# -ne 0 ] && [ ${1} == "-h" ] && [ ${1} == "--help" ] ; then
  command_usage
  exit 1
fi

if [ $# -gt 0 ]; then
  while [ x${1} != "x" ]; do
    if [ ${1} == "-3" ]; then
      shift
      t3rdmmode=1
    elif [ ${1} == "-s" ]; then
      shift
      spin=${1}
    else
      states+=(${1})
    fi
    shift
  done
fi

# if no states are specified, populate the states array with all states. the states present are extracted from the meas-4rdm.X.in files present in the directory
if [ ${#states[@]} -eq 0 ]; then
  states=($(eval 'for i in meas-4rdm.*; do b=${i%%.in}; echo ${b##meas-4rdm.}; done | sort -n'))
fi

# Extract the MOLCAS project name from the name of OneInt file. we will need it later
oneint=$(ls *.OneInt)
if [ x${oneint} != "x" ]; then
  molcasproject=${oneint%%.OneInt}
else
  echo "Cannot extract MOLCAS project. Are you sure you are in the MOLCAS scratch directory?"
  exit 1
fi

if [ ! -e FCIDUMP ]; then
  echo "FCIDUMP file not found! Exiting..."
  exit 1
fi

## for each state, populate a scratch directory
if [ $t3rdmmode -eq 0 ]; then
  echo -n "Creating scratch files for distributed 4-RDM evaluation for state "
  for state in ${states[@]}; do
    echo -n "${state} "
    dir="4rdm-scratch."${state}

    # check for the existence of the checkpoint states
    # error handling for the unfound
    checkpointstate=$(checkpoint_found ${molcasproject} ${state} ${spin})

    # if it exists, copy the necessary files into the scratch directory
    mkdir -p $dir/template
    mkdir -p $dir/parts
    cp -r $checkpointstate FCIDUMP $dir/template
    cp meas-4rdm.${state}.in $dir/template/meas-4rdm-template.in
    cp $submitfile $dir/template/
    cd $dir/template/ && sed -i "s/state=0/state=${state}/g" submit-4rdm.sh && cd ../..
    cp $jobmanager $dir/
  done
  echo "Done."
else
  echo -n "Creating scratch files for distributed t-3-RDM evaluation for states "
  submitfile=$MOLCAS/Tools/distributed-4rdm/submit-3rdm.sh
  for statei in ${states[@]}; do
    for statej in ${states[@]}; do
      if [ ${statei} -lt ${statej} ]; then
        echo -n "${statei}-${statej} "
        dir="3rdm-scratch."${statei}"."${statej}
        chkpi=$(checkpoint_found ${molcasproject} ${statei} ${spin})
        chkpj=$(checkpoint_found ${molcasproject} ${statej} ${spin})
        # again, error handling is in checkpoint_found

        mkdir -p $dir/template
        mkdir -p $dir/parts
        cp -r $chkpi $chkpj FCIDUMP $dir/template
        cp meas-3tdm.${statei}.${statej}.in $dir/template/meas-3rdm-template.in
        cp $submitfile $dir/template/
        cd $dir/template/ && sed -i "s/statei=0/statei=${statei}/g" submit-3rdm.sh && sed -i "s/statej=0/statej=${statej}/g" submit-3rdm.sh && cd ../..
        cp $jobmanager $dir/
      fi
    done
  done
fi
