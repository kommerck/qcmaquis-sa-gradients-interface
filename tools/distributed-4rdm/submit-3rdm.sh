#!/bin/bash

if [ x$LSB_DJOB_NUMPROC != "x" ]; then
  nproc=$LSB_DJOB_NUMPROC
elif [ x$PBS_NUM_PPN != "x" ]; then
  nproc=$PBS_NUM_PPN
else
  echo "Please specify the correct number of CPUs according to your batch system. Running with 1 CPU for now."
  nproc=1
fi
mycurrdir=$PWD
templatedir=$PWD/../../template
fcid=FCIDUMP
chkp=*.checkpoint_state.*.*.h5
chkptemplate=$templatedir/$chkp

statei=0
statej=0
evalrdm=$MOLCAS/External/qcmaquis_suite/src/qcmaquis/dmrg/lib/python/pyeval/transition_threeptrdm.py
resfile=*.result*.*.h5
threeptfile=threeparticle.tdm.${statei}.${statej}

### Add modules here
#module load gcc/4.9.2 szip hdf5 boost python/2.7.6 intel

rsync -aL $templatedir/$fcid $chkptemplate $mycurrdir
OMP_NUM_THREADS=$nproc dmrg_meas $mycurrdir/meas-3rdm.in > $mycurrdir/meas-3rdm.log && $evalrdm $resfile $statei $statej && sem --id lock --fg --semaphoretimeout 260 "cat $threeptfile >> ../$threeptfile"

rm -r $chkp $fcid
