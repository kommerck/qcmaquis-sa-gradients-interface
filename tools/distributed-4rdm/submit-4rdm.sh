#!/bin/bash

if [ x$LSB_DJOB_NUMPROC != "x" ]; then
  nproc=$LSB_DJOB_NUMPROC
elif [ x$PBS_NUM_PPN != "x" ]; then
  nproc=$PBS_NUM_PPN
else
  echo "Please specify the correct number of CPUs according to your batch system. Running with 1 CPU for now."
  nproc=1
fi

mycurrdir=$PWD
templatedir=$PWD/../../template
fcid=FCIDUMP
chkp=*.checkpoint_state.*.*.h5
chkptemplate=$templatedir/$chkp

state=0
evalrdm=$MOLCAS/External/qcmaquis_suite/src/qcmaquis/dmrg/lib/python/pyeval/fourptrdm_perm.py
resfile=*.result*.*.h5
fourptfile=fourparticle.rdm.$state.$state

# Load necessary modules here:
#module load gcc/4.9.2 szip hdf5 boost python/2.7.6 intel

rsync -aL $templatedir/$fcid $chkptemplate $mycurrdir
OMP_NUM_THREADS=$nproc dmrg_meas $mycurrdir/meas-4rdm.in > $mycurrdir/meas-4rdm.log && $evalrdm $resfile $state && sem --id lock --fg --semaphoretimeout 260 "cat $fourptfile >> ../$fourptfile"

rm -r $chkp $fcid
