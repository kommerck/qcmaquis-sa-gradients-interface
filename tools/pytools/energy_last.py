#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#*****************************************************************************
#*
#* ALPS MPS DMRG Project
#*
#* Copyright (C) 2015 Laboratory for Physical Chemistry, ETH Zurich
#*               2012-2015 by Sebastian Keller <sebkelle@phys.ethz.ch>
#*               2015-2015 modified by Yingjin
#*
#*
#* This software is part of the ALPS Applications, published under the ALPS
#* Application License; you can use, redistribute it and/or modify it under
#* the terms of the license, either version 1 or (at your option) any later
#* version.
#*
#* You should have received a copy of the ALPS Application License along with
#* the ALPS Applications; see the file LICENSE.txt. If not, the license is also
#* available from http://alps.comp-phys.org/.
#*
#* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
#* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
#* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
#* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#* DEALINGS IN THE SOFTWARE.
#*
#*****************************************************************************


# usage: energy.py h5-result-file
# small interactive script to print groundstate energy

import pydmrg
import sys
import numpy as np

def energy_last(fname):

    ret = pydmrg.LoadDMRGSweeps([fname],['Energy'])

    sweeps = []
    for sw in ret[0]:
        sweeps += list(sw[0].y)

    ydata = np.array(sweeps)

    if np.min(ydata.imag) != 0:
        print "Warning! complex energy value detected"

    print "Last sweep energy:"
#    print np.min(ydata.real)
    print np.array(sweeps)[-1]
    print "Number of sweeps"
    nsweeps = len(ret[0])
    print nsweeps

    ret = pydmrg.LoadDMRGSweeps([fname],['TruncatedWeight'])

    print "truncated weight accumulated over all sweeps"
    # get the truncated weight for every sweep, returns a list of lists
    sweeps2=[list(sw[0].y) for sw in ret[0]]
    # flatten the list of lists to 1D list
    sw_flat=[item for sublist in sweeps2 for item in sublist]
    # sum over all elements
    print np.sum(sw_flat)
#   print ydata_last
#   print swl

if __name__=='__main__':
    rfile = sys.argv[1]
    energy_last(rfile)
