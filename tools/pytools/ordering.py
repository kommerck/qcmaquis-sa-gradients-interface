#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#*****************************************************************************
#*
#* ALPS MPS DMRG Project
#*
#* Copyright (C) 2013 Laboratory for Physical Chemistry, ETH Zurich
#*               2014-2015 by Sebastian Keller <sebkelle@phys.ethz.ch>
#*               2015-2015 by yma <yma@ethz.ch>
#*
#* 
#* This software is part of the ALPS Applications, published under the ALPS
#* Application License; you can use, redistribute it and/or modify it under
#* the terms of the license, either version 1 or (at your option) any later
#* version.
#* 
#* You should have received a copy of the ALPS Application License along with
#* the ALPS Applications; see the file LICENSE.txt. If not, the license is also
#* available from http://alps.comp-phys.org/.
#*
#* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
#* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
#* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT 
#* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE 
#* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE, 
#* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
#* DEALINGS IN THE SOFTWARE.
#*
#*****************************************************************************


#usage : ordering.py results_state.x.h5 

import sys                
import numpy as np
import input as DmrgInput

#main program
if __name__ == '__main__':
    inputfile = sys.argv[1]

    props = DmrgInput.loadProperties(inputfile)

    norb = (props["L"])

    if 'orbital_order' in props:
#       print "found the ordering, it is "
        original_order = map(int, props["orbital_order"].split(','))
    else:
#       print "No ordering specified, use the default ordering instead"
        original_order = []        
        for i in range(norb):
           original_order.append(i+1)

    # Change for your format 
    print original_order

    
