#!/usr/bin/env python2
#
# DMRG driver script, written and maintained
# by Stefan Knecht and Yingjin Ma
# ETH Zurich 2013-2016.
# Modifications 2017,2018: Leon Freitag and Christopher Stein
#
# template based on original "wrapper" script written by Ulf Ekstroem (U Oslo, Norway).
#
# For suggestions/bug reports: stefan.knecht@phys.chem.ethz.ch
#

import os, subprocess, shutil, glob, signal, optparse, sys, shlex, tempfile, zipfile, platform

def runDMRG():
    usage = """%prog [options] INPUT
Run a DMRG calculation starting from the given input file. Additional files will be copied
to the temporary directory. If the input file is based on a template use the --replace="NAME=VALUE" option
to set the appropriate keyword.

Use --launcher="mpirun OPTIONS" with the appropriate OPTIONS to run an mpi job.

"""
    dmrg = DMRGRUN()
    parser = optparse.OptionParser(usage)

    parser.add_option('-v','--verbose', action='store_true', dest="verbose",
		     help='Print more information')

    # General Options
    group = optparse.OptionGroup(parser, 'General Options')
    group.add_option('--executable', type='string', default=None, dest="executable",
                     help='Path to the main executable',metavar="FILE")
    group.add_option('-D','--define', type='string', action='callback',callback=dmrg.define_callback,
		     help='Define the environment variable NAME as VALUE',
                     metavar='NAME=VALUE')
    group.add_option('-i','--put', type='string', action='callback',callback=dmrg.infile_callback,
		     help='Copy FILE to the temporary directory, optionally renaming it to NAME',
                     metavar='[NAME=]FILE]')
    group.add_option('-r','--replace', type='string', action='append',default=[],dest="replace",
         	     help='Replace NAME by VALUE in the input file',metavar="NAME=VALUE")
    group.add_option('-g','--get', type='string', action='callback',callback=dmrg.getfile_callback,
		     help='Copy FILE from temporary directory after the calculation',
                     metavar='NAME[=FILE]')
    group.add_option('--output', type='string', default=None, dest="outputfile",
                     help='Put output of running program in this file.',metavar="FILE")
    group.add_option('-T','--tmpfull', type='string', default=None,dest="tmppath",
		     help='Full path to temporary directory. Will be created if not there.',metavar="PATH")
    parser.add_option_group(group)

    # Parallel Options
    group = optparse.OptionGroup(parser, 'Parallel Options')
    group.add_option('--launcher', type='string', default=None,dest="launcher",
		     help='Command used to launch the main executable, i.e. "mpirun -np 2"',metavar="COMMAND")
    parser.add_option_group(group)

    # Non-Standard Options
    group = optparse.OptionGroup(parser, 'Non-Standard Options')
    group.add_option('--tdm', action='store_true', dest="tdm",default=False,
		         help='Use separate DMRG input file for x-tdm measurements')
    group.add_option('--tdmlevel', type='string', dest="tdmlevel",default=None,
		         help='Determines the excitation rank x for x-TDM measurements')
    group.add_option('--overlap', action='store_true', dest="overlap",
		         help='Run an overlap calculation')
    group.add_option('--cideas', action='store_true', dest="cideas",default=False,
		         help='Use CI-DEAS start guess for DMRG calculation')
    group.add_option('--rotate', action='store_true', dest="MPSrotate",default=False,
		         help='Perform an MPS counterrotation calculation')
    group.add_option('--notransform', action='store_false', dest="MPStransform",default=True,
		         help='Perform an MPS counterrotation calculation')
    group.add_option('--transformSPIN', action='store_true', dest="MPStransformSPIN",default=False,
		         help='Perform an MPS transformation from SU2 to 2U1 representation')
    group.add_option('--lhs', type='string', dest="lhs",default=None,
		         help='bra MPS for overlap/TDM calculation')
    group.add_option('--rhs', type='string', dest="rhs",default=None,
		         help='ket MPS for overlap/TDM calculation')
    group.add_option('--msprojRHS', type='string', dest="msprojrhs",default=None,
		         help='Ms projection of ket MPS for TDM calculation')
    group.add_option('--msprojLHS', type='string', dest="msprojlhs",default=None,
		         help='Ms projection of bra MPS for TDM calculation')
    group.add_option('--input',type='string',dest='dmrginputfile',default=None,
             help='Set QCMaquis input file name')
    group.add_option('--input-only',type='string',dest='dmrginput',default=None,
             help='Create input files named DMRGINPUT for QCMaquis only without running the DMRG binary.')
    group.add_option('--compress',type='string',dest='mpscompress',default=None,
                     help='Compress an MPS (specify the ket MPS with --rhs)')
    parser.add_option_group(group)


    # Read .rc files
    rcargs = []
    rcpaths = ['dmrgrc','dmrgrc',
               os.path.join(os.path.expanduser(''),'dmrgrc')]
    for path in rcpaths:
        try:
            f = open(path,'r')
            for l in f.readlines():
                l = l.strip()
                if len(l) > 0:
                    if l[0] != '#':
                        for a in shlex.split(l):
                            rcargs.append(os.path.expandvars(a))
            break
        except Exception:
            pass

    (options, args)       = parser.parse_args(rcargs+sys.argv[1:])
    dmrg.verbose          = options.verbose
    dmrg.executable       = options.executable
    dmrg.launcher         = options.launcher
    dmrg.tdm              = options.tdm
    dmrg.tdmlevel         = options.tdmlevel
    dmrg.overlap          = options.overlap
    dmrg.cideas           = options.cideas
    dmrg.tmppath          = options.tmppath
    dmrg.outputfile       = options.outputfile
    dmrg.lhs              = options.lhs
    dmrg.rhs              = options.rhs
    dmrg.msprojlhs        = options.msprojlhs
    dmrg.msprojrhs        = options.msprojrhs
    dmrg.MPSrotate        = options.MPSrotate
    dmrg.MPStransform     = options.MPStransform
    dmrg.MPStransformSPIN = options.MPStransformSPIN
    dmrg.inputonly        = not (options.dmrginput is None)
    dmrg.mpscompress      = not (options.mpscompress is None)

    # stknecht: for debugging purposes
    #dmrg.verbose    = True
    if dmrg.verbose: print "runDMRG: all arguments '%s'" % sys.argv[0:]

    if not dmrg.inputonly:
      #set up job name
      jobname     = 'dmrg'
      if dmrg.overlap:
          jobname = 'overlap'
      if dmrg.outputfile is not None:
          if dmrg.overlap:
            stdout_file_name = 'stdout.overlap.out'
          else:
            stdout_file_name = dmrg.outputfile
      else:
          stdout_file_name = jobname + '.out'

      dmrg.stdout = open(stdout_file_name,'w')
      dmrg.stderr = open(jobname+'.err','w')

    dmrg.stdin  = None
    inppath     = None

    # input preparations
    if dmrg.tdm:
        dmrg.dmrginput = 'dmrg-input-tdm'
    elif dmrg.overlap:
        dmrg.dmrginput = dmrg.lhs + ' ' + dmrg.rhs + ' > ' + dmrg.outputfile
    elif dmrg.MPSrotate:
        #dmrg.dmrginput = sys.argv[len(sys.argv)-1]
         dmrg.dmrginput = ' '
    elif dmrg.MPStransformSPIN:
         dmrg.dmrginput = ' '
    elif dmrg.mpscompress:
         dmrg.dmrginput = ' '
    else:
      if not (options.dmrginputfile is None):
        dmrg.dmrginput = options.dmrginputfile

    cmd_trafo = ' '
    if platform.system() == 'Darwin': cmd_trafo += 'export DYLD_LIBRARY_PATH=$LIBRARY_PATH && '

    if dmrg.MPSrotate:

        # RHS MPS
        print 'right-hand-side for trafo is ',dmrg.rhs
        cmd_trafo_rhs = cmd_trafo + ' mps_transform_pg ' + dmrg.rhs
        if  dmrg.msprojrhs is not None:
            cmd_trafo_rhs += ' ' + dmrg.msprojrhs

        t = subprocess.Popen(cmd_trafo_rhs,stdin=dmrg.stdin,stdout=dmrg.stdout,stderr=dmrg.stderr,shell=True,
                             preexec_fn=os.setsid,env=os.environ.copy())
        pgid = t.pid
        t.wait() # OSError will be raised here if killed
        if t.returncode != 0:
            raise subprocess.CalledProcessError(t.returncode,cmd_trafo_rhs)

        if dmrg.verbose:
            print "runDMRG: MPS transformation (prior to rotation) successful"

        lines = [line.rstrip('\n') for line in open('mpstransform.txt')]
        os.remove('mpstransform.txt')

        # set input MPS for rotation
        dmrg.dmrginput += ' ' + lines[0]

    if dmrg.MPStransformSPIN:
        # RHS MPS
        cmd_trafo_rhs = cmd_trafo + ' ' + dmrg.executable + ' ' + dmrg.rhs

        t = subprocess.Popen(cmd_trafo_rhs,stdin=dmrg.stdin,stdout=dmrg.stdout,stderr=dmrg.stderr,shell=True,
                             preexec_fn=os.setsid,env=os.environ.copy())
        pgid = t.pid
        t.wait() # OSError will be raised here if killed
        if t.returncode != 0:
            raise subprocess.CalledProcessError(t.returncode,cmd_trafo_rhs)

        if dmrg.verbose:
            print "runDMRG: MPS transformation successful"

        lines = [line.rstrip('\n') for line in open('mpstransform.txt')]
        os.remove('mpstransform.txt')
        sys.exit(0)
    if dmrg.mpscompress:
    # MPS compression
        cmd_compress = cmd_trafo + ' ' + dmrg.executable + ' ' + dmrg.rhs + ' ' + options.mpscompress

        t = subprocess.Popen(cmd_compress,stdin=dmrg.stdin,stdout=dmrg.stdout,stderr=dmrg.stderr,shell=True,
                             preexec_fn=os.setsid,env=os.environ.copy())
        t.wait() # OSError will be raised here if killed
        if t.returncode != 0:
          raise subprocess.CalledProcessError(t.returncode,cmd_compress)

        if dmrg.verbose:
          print "runDMRG: MPS compression successful"
        sys.exit(0)
    if dmrg.cideas:
    # MPS compression
        cmd_cideas = cmd_trafo + ' ' + dmrg.executable + ' ' + dmrg.dmrginput

        t = subprocess.Popen(cmd_cideas,stdin=dmrg.stdin,stdout=dmrg.stdout,stderr=dmrg.stderr,shell=True,
                             preexec_fn=os.setsid,env=os.environ.copy())
        t.wait() # OSError will be raised here if killed
        if t.returncode != 0:
          raise subprocess.CalledProcessError(t.returncode,cmd_cideas)

        if dmrg.verbose:
          print "runDMRG: CIDEAS successful"
        sys.exit(0)

    # if TDM (measured in 2u1pg) take care of a proper transformation of the bra/ket MPS before the measurement
    if dmrg.tdm:

        if dmrg.MPStransform:

            # RHS MPS
            cmd_trafo_rhs = cmd_trafo + ' mps_transform_pg ' + dmrg.rhs
            if  dmrg.msprojrhs is not None:
                cmd_trafo_rhs += ' ' + dmrg.msprojrhs

            t = subprocess.Popen(cmd_trafo_rhs,stdin=dmrg.stdin,stdout=dmrg.stdout,stderr=dmrg.stderr,shell=True,
                                 preexec_fn=os.setsid,env=os.environ.copy())
            pgid = t.pid
            t.wait() # OSError will be raised here if killed
            if t.returncode != 0:
                raise subprocess.CalledProcessError(t.returncode,cmd_trafo_rhs)

            if dmrg.verbose:
                print "runDMRG: Executed rhs-MPS transformation successfully"

            lines = [line.rstrip('\n') for line in open('mpstransform.txt')]
            os.remove('mpstransform.txt')

            # LHS MPS
            if dmrg.lhs !=  dmrg.rhs:
                cmd_trafo_lhs = cmd_trafo + ' mps_transform_pg ' + dmrg.lhs
                if  dmrg.msprojlhs is not None:
                    cmd_trafo_lhs += ' ' + dmrg.msprojlhs
                t = subprocess.Popen(cmd_trafo_lhs,stdin=dmrg.stdin,stdout=dmrg.stdout,stderr=dmrg.stderr,shell=True,
                                     preexec_fn=os.setsid,env=os.environ.copy())
                pgid = t.pid
                t.wait() # OSError will be raised here if killed
                if t.returncode != 0:
                    raise subprocess.CalledProcessError(t.returncode,cmd_trafo_lhs)
                if dmrg.verbose:
                    print "runDMRG: Executed lhs-MPS transformation successfully"

                linesL = [line.rstrip('\n') for line in open('mpstransform.txt')]
                os.remove('mpstransform.txt')
                options.replace.append('bra_checkpoint='+linesL[0])

            else:#dmrg.lhs ==  dmrg.rhs:
                options.replace.append('bra_checkpoint='+lines[0])

            options.replace.append('saved_checkpoint='+lines[0])
            options.replace.append('isup='+lines[1])
            options.replace.append('isdown='+lines[2])
            options.replace.append('isirrep='+lines[3])

        if dmrg.tdmlevel == '1':
            options.replace.append('MEASURE[trans2rdm=//MEASURE[trans2rdm')
        elif dmrg.tdmlevel == '2':
            options.replace.append('MEASURE[trans1rdm=//MEASURE[trans1rdm')
        elif dmrg.tdmlevel == '3':
            print >> sys.stderr, "runDMRG: automatized 3-TDM measurements not available yet in this version"
            sys.exit(-1)

    def rmsuffix(a,ss):
        "If a ends in one of the strings in ss, return a with the suffix removed, else return None"
        for s in ss:
            if a[-len(s):] == s:
                return a[:-len(s)]
        return None

    # Detect file types in args from the file names
    for f in args:
        if rmsuffix(f,['.input','-input','.conf','rdm1','rdm2','.maquis','model','parms']):
            if inppath is not None:
                print >> sys.stderr, "runDMRG: Multiple input files given, aborting."
                sys.exit(-1)
            inppath = f
        else: # Just copy in unknown files
            dmrg.copyin.append([f])
    if inppath is None:
        if not (dmrg.overlap or dmrg.MPSrotate):
            print >> sys.stderr, "runDMRG: no input file given, aborting."
            sys.exit(-1)

    replacements = reduce(list.__add__,[x.split("=") for x in options.replace],[])
    if inppath is not None:
        dmrg.copyin.append([inppath,dmrg.dmrginput]+replacements)


    # Append executable itself to the commands to be run
    if options.executable is None:
      if options.dmrginput is None:
        print >> sys.stderr,"runDMRG: No executable given (with --executable=), aborting."
        sys.exit(-1)

    else:
        exename = options.executable
        if dmrg.launcher is None:
            dmrg.cmdlines.append(['export LC_ALL=C && '+exename+' '+' '+dmrg.dmrginput])
        else:
            dmrg.cmdlines.append(['export LC_ALL=C && ']+shlex.split(dmrg.launcher)+[exename+' '+' '+dmrg.dmrginput])

    # Now start the real work
    try:
        dmrg.setup()
    except:
        print >> sys.stderr, "runDMRG: setup failed"
        raise
    try:
        dmrg.init()
        try:
            dmrg.run()
        except:
            print >> sys.stderr, "runDMRG: DMRG run failed"
            raise
        finally:
            dmrg.finalize()
    except:
        print >> sys.stderr, "runDMRG: DMRG init/run failed"
        raise
    finally:
        if not dmrg.inputonly:
          dmrg.cleanup()
          dmrg.stdout.close()
          dmrg.stderr.close()
          if os.path.getsize(jobname+'.err') == 0:
              os.remove(jobname+'.err')

class DMRGRUN:
    def __init__(self):
        # User modifiable variables
        self.tmpbase      = None
        self.tmppath      = None
        self.stdin        = None
        self.stdout       = None
        self.stderr       = None
        self.cmdlines     = []
        self.copyin       = []
        self.copyout      = []
        self.extract      = []
        self.executable   = None
        self.launcher     = None
        self.verbose      = False
        self.overlap      = False
        self.workdir      = None
        self.env          = os.environ.copy()
        self.dmrginput    = 'dmrg-input'
        self.rhs          = ''
        self.lhs          = ''
        self.inputonly    = False
        self.mpscompress  = False

    def define_callback(self, option, opt, value, parser):
	if '=' in value:
	    ff = value.split('=')
            self.env[ff[0]] = '='.join(ff[1:])
	else:
	    print >> sys.stderr, "runDMRG: Illegal argument to",opt,"expected NAME=VALUE"
	    sys.exit(1)

    def infile_callback(self, option, opt, value, parser):
	if '=' in value:
	    ff = value.split('=')
	    if len(ff) != 2:
                parser.error("Illegal argument to "+opt)
		print >> sys.stderr, "runDMRG: Illegal argument to",opt
		sys.exit(1)
	    self.copyin.append(ff)
	else:
	    self.copyin.append([value])

    def getfile_callback(self, option, opt, value, parser):
	if '=' in value:
	    ff = value.split('=')
	    if len(ff) != 2:
		print >> sys.stderr, "runDMRG: Illegal argument to",opt
		sys.exit(1)
	    self.copyout.append(ff)
	else:
	    self.copyout.append([value])

    def setup(self):
        try:
            if self.tmppath is not None:
                self.tmppath = os.path.abspath(self.tmppath)
                if os.path.isdir(self.tmppath):
                    self.keeptmp = True
                else:
                    os.makedirs(self.tmppath)
            else:
                if self.tmpbase is None:
                    self.tmppath = tempfile.mkdtemp()
                else:
                    self.tmppath = tempfile.mkdtemp(dir=self.tmpbase)
            if self.verbose: print "runDMRG: Using temporary directory '%s'" % self.tmppath
            self.workdir = os.getcwd()
        except:
            print >> sys.stderr, "runDMRG: Cannot create temporary directory '%s', quitting." % self.tmppath
            raise

    def init(self):
        "Copy files and possibly perform replacements within a given file."
	if self.overlap:
            return
        # Copy in files, stop if problem
        for f in self.copyin:
            src = f[0]
            # skip dmrg-input and executables
            if src in ('dmrg-input','dmrg-input-tdm','onetdm_diag_2u1pg','dmrg_meas','dmrg'):
                continue
            if os.path.isfile(src):
                if len(f) % 2 == 0: # dst name given
                    dst = f[1]
                    rep = f[2:]
                else:
                    dst = os.path.join(self.tmppath,os.path.basename(src))
                    rep = f[1:]
                rep = zip(rep[0::2],rep[1::2])
                try:
                    self.copy_with_replacement(src,dst,rep)
                except:
                    print >> sys.stderr, "runDMRG: File copy failed, check output. (pass)"
                    pass
            else:
                print >> sys.stderr, "runDMRG: File copy of %s failed, source file nonexistent." % src

    def copy_with_replacement(self,src,dst,rep=[]):
        try:
            if self.verbose: print "runDMRG: Copying '"+src+"' to '"+dst+"'"
            if len(rep) == 0:
                shutil.copy2(src,dst)
            else:
                if self.verbose:
                    for r in rep:
                        print "runDMRG: While replacing '"+r[0]+"' by '"+r[1]+"'"
                of = open(dst,'w')
                for l in open(src,'r').readlines():
                    for r in rep:
                        l = l.replace(r[0],r[1])
                    t = 0
                    for b in l.split():
                        if b == '"delete-line"' or b == 'delete-line':
                            t = 1
                    if t != 1: of.write(l)
                of.close()
        except IOError,e:
            print >> sys.stderr, "runDMRG: Error copying file '"+src+"'"
            raise

    def finalize(self):
        os.chdir(self.tmppath)
        # Copy back files
        def cpout(src,dst=None):
            if self.verbose: print "runDMRG: Copying back",src
            if os.path.isfile(src):
                if dst is None: dst='.'
                shutil.copy2(src,dst)
            elif os.path.isdir(src):
                if dst is None: dst=os.path.basename(src)
                if os.path.exists(dst):
                    shutil.rmtree(dst)
                shutil.copytree(src,dst)
            elif self.verbose:
                print "runDMRG: When copying out, file",src,"not found."
        os.chdir(self.workdir)
        for f in self.copyout:
            try:
                if len(f) == 2:
                    cpout(os.path.join(self.tmppath,f[0]),f[1])
                else: # allow patterns in copyout if there is no =
                    for ff in glob.glob(os.path.join(self.tmppath,f[0])):
                        cpout(ff)
            except:
                print >> sys.stderr,"runDMRG: Error copying out file",f
                raise

    def cleanup(self):
        os.chdir(self.workdir)
        if self.verbose:
            print "runDMRG: Temporary directory deleted"
    def run_cmd(self,cmd,silent=False):
        "Run command line cmd (a list of arguments), return exit code"
        if self.inputonly:
          # don't run anything if inputonly is set
	  return
        pgid = None
        oldhand = []
        try:
            cmd_list = cmd
            if platform.system() == 'Darwin':
                cmd      = 'export DYLD_LIBRARY_PATH=$LIBRARY_PATH && ' + ' '.join(cmd_list)
            else:
                cmd      = ' '.join(cmd_list)
            def handler(signum,frame):
                if pgid is not None:
                    if signum == signal.SIGALRM: signum = signal.SIGTERM
                    os.killpg(pgid,signum)
            catch = [signal.SIGTERM, signal.SIGHUP, signal.SIGINT, signal.SIGALRM]
            for s in catch:
                oldhand.append(signal.signal(s,handler))
            if self.verbose:
                print "runDMRG: Executing",cmd

            p = subprocess.Popen(cmd,stdin=self.stdin,stdout=self.stdout,stderr=self.stderr,shell=True,
                                 preexec_fn=os.setsid,env=self.env)
            pgid = p.pid
            p.wait() # OSError will be raised here if killed
            if p.returncode != 0:
                raise subprocess.CalledProcessError(p.returncode,cmd)
        except subprocess.CalledProcessError, e:
            if not silent:
                print >> sys.stderr, "runDMRG: Command "+str(cmd)+" failed with returncode",e.returncode
            raise
        except OSError, e: # Typical signal
            if not silent:
                print >> sys.stderr, "runDMRG: Got OSError %i while executing" % e.errno,cmd
            raise subprocess.CalledProcessError(e.errno,cmd)
        finally: # restore signal handlers
            if len(oldhand) > 0:
                for s,h in zip(catch,oldhand):
                    signal.signal(s,h)

    def run(self):
        try:
            for cmd in self.cmdlines:
                self.run_cmd(cmd)
        finally:
            os.chdir(self.workdir)

runDMRG()
