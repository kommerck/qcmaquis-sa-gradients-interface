#!/usr/bin/env python2
#-*- coding: utf-8 -*-
#*****************************************************************************
#*
#* ALPS MPS DMRG Project
#*
#* Copyright (C) 2015 Laboratory for Physical Chemistry, ETH Zurich
#*               2015-2015 by Stefan Knecht <stefan.knecht@phys.chem.ethz.ch>
#*
#* 
#* This software is part of the ALPS Applications, published under the ALPS
#* Application License; you can use, redistribute it and/or modify it under
#* the terms of the license, either version 1 or (at your option) any later
#* version.
#* 
#* You should have received a copy of the ALPS Application License along with
#* the ALPS Applications; see the file LICENSE.txt. If not, the license is also
#* available from http://alps.comp-phys.org/.
#*
#* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
#* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
#* FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT 
#* SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE 
#* FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE, 
#* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
#* DEALINGS IN THE SOFTWARE.
#*
#*****************************************************************************

import sys
import pyalps
import numpy as np

def assemble_corr(diag, triangdouble):
    """From the diagonal, upper and lower triangle, construct a symmetric matrix
       diag: diagonal
       triangdoouble: upper and lower triangle, sequential reversed rows"""

    L = len(diag.y)
    assert((L-1)*L == len(triangdouble.y[0]))

    ret = np.zeros((L,L))

    # set the diagonal
    for lab, val in zip(diag.x, diag.y):
        i = lab
        ret[i,i] = val

    for lab, val in zip(triangdouble.x, triangdouble.y[0]):
        i = lab[0]
        j = lab[1]
        ret[i,j] = val

    return ret


def load_1tdm_xx(inputfile,tag_diag,tag_offdiag):
    """From the diagonal and upper and lower triangle, construct a symmetric matrix
       diag: diagonal
       triang: upper and triangle, sequential reversed rows"""
    
    diagup    = pyalps.loadEigenstateMeasurements([inputfile], what=tag_diag)[0][0]
    dxxtriang = pyalps.loadEigenstateMeasurements([inputfile], what=tag_offdiag)[0][0]

#   print 'diagonal part of %s TDM ' % (tag_diag)
#   for lab, val in zip(diagup.x, diagup.y):
#       i = lab
#       j = i
#       print i, j, val

#   print ' %s part of TDM' % (tag_offdiag)
#   for lab, val in zip(dxxtriang.x, dxxtriang.y[0]):
#       i = lab[0]
#       j = lab[1]
#       print i, j, val

    # Create the full matrix from the diagonal (diagup.y) and upper triangle (dxxtriang)
    dxx = assemble_corr(diagup, dxxtriang)

#   print dxx

    return dxx

def load_2tdm_xxxx(inputfile,tag):

    rdm      =  pyalps.loadEigenstateMeasurements([inputfile], what=tag)[0][0]
    rdm.y[0] = 0.5 * rdm.y[0]
    L        = rdm.props['L']

    return (rdm,int(L))

def save_1tdm(tdmaa,tdmab,tdmba,tdmbb,tag1,tag2):
    L = len(tdmaa)
    fmt = '%14.14e'

    # fix correct naming with tag1/tag2
    f=open('oneparticle.tdm.%s.%s' % (tag1, tag2),'w')
    f.write(str(L)+'\n')

    for i in range(L):
        for j in range(L):
	    f.write(str(i)+' '+str(j)+' '+str(fmt%tdmaa[i,j])+'\n')	
    for i in range(L):
        for j in range(L):
	    f.write(str(i)+' '+str(j)+' '+str(fmt%tdmab[i,j])+'\n')	
    for i in range(L):
        for j in range(L):
	    f.write(str(i)+' '+str(j)+' '+str(fmt%tdmba[i,j])+'\n')	
    for i in range(L):
        for j in range(L):
	    f.write(str(i)+' '+str(j)+' '+str(fmt%tdmbb[i,j])+'\n')	
    f.close()	    
 
    return L

def save_2tdm(tdmaaaa,tdmabba,tdmbaab,tdmbbbb,tag1,tag2,L):


    # file suffixes according to tag1/tag2
    fmt = '%14.14e'
    f   =open('twoparticle.tdm.%s.%s' % (tag1, tag2),'w')
    f.write(str(L)+'\n')

    for lab, val in zip(tdmaaaa.x, tdmaaaa.y[0]):
        i = lab[0]
        j = lab[1]
        k = lab[2]
        l = lab[3]
        f.write(str(i)+' '+str(j)+' '+str(k)+' '+str(l)+' '+str(fmt%val)+'\n')
        if l!=k:
            f.write(str(j)+' '+str(i)+' '+str(l)+' '+str(k)+' '+str(fmt%val)+'\n')

    for lab, val in zip(tdmabba.x, tdmabba.y[0]):
        i = lab[0]
        j = lab[1]
        k = lab[2]
        l = lab[3]
        f.write(str(i)+' '+str(j)+' '+str(k)+' '+str(l)+' '+str(fmt%val)+'\n')
        if l!=k:
            f.write(str(j)+' '+str(i)+' '+str(l)+' '+str(k)+' '+str(fmt%val)+'\n')

    for lab, val in zip(tdmbaab.x, tdmbaab.y[0]):
        i = lab[0]
        j = lab[1]
        k = lab[2]
        l = lab[3]
        f.write(str(i)+' '+str(j)+' '+str(k)+' '+str(l)+' '+str(fmt%val)+'\n')
        if l!=k:
            f.write(str(j)+' '+str(i)+' '+str(l)+' '+str(k)+' '+str(fmt%val)+'\n')

    for lab, val in zip(tdmbbbb.x, tdmbbbb.y[0]):
        i = lab[0]
        j = lab[1]
        k = lab[2]
        l = lab[3]
        f.write(str(i)+' '+str(j)+' '+str(k)+' '+str(l)+' '+str(fmt%val)+'\n')
        if l!=k:
            f.write(str(j)+' '+str(i)+' '+str(l)+' '+str(k)+' '+str(fmt%val)+'\n')

    f.close()	    
 
if __name__ == '__main__':

    assert(len(sys.argv) >= 5)

    inputfile = sys.argv[1]
    tag1      = sys.argv[2]
    tag2      = sys.argv[3]
 
#   print 'input parameters are: inputfile = %s, tag1 = %s, tag2 = %s, switch = %s' % (inputfile, tag1, tag2, sys.argv[4])
    if sys.argv[4] == '1':

        tdmaa = load_1tdm_xx(inputfile,'count_aa_1TDM','transition_oneptdm_aa')
        tdmab = load_1tdm_xx(inputfile,'a2b_1TDM',     'transition_oneptdm_ab')
        tdmba = load_1tdm_xx(inputfile,'b2a_1TDM',     'transition_oneptdm_ba')
        tdmbb = load_1tdm_xx(inputfile,'count_bb_1TDM','transition_oneptdm_bb')
        L     = save_1tdm(tdmaa,tdmab,tdmba,tdmbb,tag1,tag2)

    elif sys.argv[4] == '2':
        (tdmaaaa,L) = load_2tdm_xxxx(inputfile,'transition_twoptdm_aaaa')
        (tdmabba,L) = load_2tdm_xxxx(inputfile,'transition_twoptdm_abba')
        (tdmbaab,L) = load_2tdm_xxxx(inputfile,'transition_twoptdm_baab')
        (tdmbbbb,L) = load_2tdm_xxxx(inputfile,'transition_twoptdm_bbbb')

        save_2tdm(tdmaaaa,tdmabba,tdmbaab,tdmbbbb,tag1,tag2,L)

